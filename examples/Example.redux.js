// редакс-модули могу размещаться:
// - в папке компонента components/Component/Component.redux.js
// - в папке страницы pages/Page/Page.redux.js
// - в директории redux/modules/example.js, если он используеся в нескольких местах

import API from 'constants/api';
import { mergeState, createReducer } from 'helpers/redux';
import axios from 'utils/request';
import { getApiUrl } from 'utils/urlBuilder';

import normalize from './normalize';

/**
 * Константы
 */
// именование:  МодульСущностьДейтсвие
// значение: [...][success][error] для визуализации важных экшенов
export const EXAMPLE_FETCH_SUCCESS = 'example/FETCH [success]';
export const EXAMPLE_FETCH_ERROR = 'example/FETCH [error]';
export const EXAMPLE_USER_NOTES_FETCH = 'example/USER_NOTES_FETCH';
export const EXAMPLE_ACTIVE_TAB_SET = 'example/ACTIVE_TAB_SET';

const initialState = {};

/**
 * Экшен-криаторы
 */

// простой экшен-криатор
// мы используем thunk, поэтому должен возвращаться либо объект
// по спецификации FSA (https://github.com/acdlite/flux-standard-action)
// либо функция
export const exampleWhateverToDo = (tab) => ({
  type: EXAMPLE_ACTIVE_TAB_SET,
  payload: tab,
});

// можно и так
export function exampleSetActiveTab(tab) {
  return {
    type: EXAMPLE_ACTIVE_TAB_SET,
    payload: tab,
  };
}

/**
 * Запрос данных для рендеринга страницы
 * - вызывается только из роут-модуля
 * - изоморфный
 * - обязательное кеширование данных в сторе по ключу с проверкой
 */
export const exampleFetch = (id) => (dispatch, getState) => {
  const exampleStored = getState().example[id];

  if (exampleStored && exampleStored.error) {
    // возвращаем ощибку с нужным кодом для корректной отрисовки страницы ошибки
    return Promise.reject({
      message: exampleStored.error
    });
  } else if (exampleStored) {
    return Promise.resolve();
  }

  // формируем урл с именованными параметрами
  const url = getApiUrl(API.GET_EXAMPLE, { id });

  // - возвращаем промис
  // - можно использовать async/await нотацию

  // ВАЖНО: НЕ НУЖНО диспатчить экшен прогресса
  // т.к. обработкой перехода по страницам занимается роут-модуль и client.js

  return axios
    .get(url)
    // https://github.com/axios/axios#response-schema
    // http://[dev-]{examples}-service.domclick.ru/apidocs.json + swagger ui console или
    // http://[dev-]{examples}-service.domclick.ru/apidocs
    .then(({ data: { result } }) => dispatch({
      type: EXAMPLE_FETCH_SUCCESS,
      payload: {
        // данные в сторе храним в нормализованном виде, но без форматирования
        [id]: normalize(result),
      },
    }))
    // если не обрабатывать ошибку тут, то она всплывет выше в роут-модуль и будет и должна быть обработана в нем
    .catch((error) => {
      // если ошибка пришла от сервера, то error.message содержит код ответа от сервера (400, 401.. 500..)
      // если ошибка произошла в обработчике ответа (чаще всего - в нормалайзере:), то текст ошибки
      dispatch({
        type: EXAMPLE_FETCH_ERROR,
        // для типового редьюсера пейлоад формируем в том виде, в котором он будет храниться в сторе
        payload: {
          [id]: {
            error: error.message,
          }
        },
      });

      // обязательно проблрасываем ошибку вверх, чтобы роут-модуль смог ее обработать
      throw new Error(error.message);
    });
};

/**
 * Запрос пользовательских/дополнительных данных
 * - выполнятся в компоненте (componentDidMount)
 * - по возможности кешируется
 */
export const exampleUserNotesFetch = (id) => (dispatch) => {
  const url = getApiUrl(API.GET_EXAMPLE_USER_NOTES);

  // формируем урл с параметрами через ?
  return axios
    .get(url, {
      query: {
        id,
      },
    })
    .then(({ data: result }) => result)
    .catch(() => [])
    // данные не критичные, поэтому в случае ошибки (например, если нет заметок, то ручка возвращает 404)
    // перехватываем ее и возвращем пустой результат
    .then((items) => dispatch({
      type: EXAMPLE_USER_NOTES_FETCH,
      // специфичный редьюсер - сам определит структуру данных для стора
      payload: items,
      meta: {
        id,
      },
    }));
};

/**
 * Специфичный редьюсер
 */
const onOfferUserNotesFtech = (state, { payload, meta: { id } }) => ({
  ...state,
  [id]: {
    ...state[id],
    notes: payload,
  }
});

// mergeState просто вмерживает payload и meta в текущую ветку стора
// createReducer проверяет сущестования обработчика по заданному ключу и запускает его
export default createReducer({
  [EXAMPLE_FETCH_SUCCESS]: mergeState,
  [EXAMPLE_FETCH_ERROR]: mergeState,
  [EXAMPLE_USER_NOTES_FETCH]: onOfferUserNotesFtech,
}, initialState);
