// порядок импортов:
// - реакт модули в альфавитном порядке
// - модули из node_modules
// - абсолютно импортируемые модули проект
// - относительно испортируемые модули проекта

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import css from 'react-css-modules';

import { shapeOfferData } from 'helpers/prop-types';

import { fetchExtraData } from './Example.redux';
import * as metrics from './Example.metrics';
import config from './Exampl.config';
import OfferDetails from './OfferDetails';

// рекомендуется использовать css-modules
import styles from './styles/Example.module.less';

// рекомендуется выносить mapStateToProps в отельную константу
const mapStateToProps = (state, ownProps) => {
  const { params: { id: offerId } } = ownProps;
  const { data, data: { id }, } = state.offer[offerId];

  return {
    data,
    house: state.house[data.guid],
    id,
  };
};

// все компоненрты лежат в папке components, кроме тех, к которым привязан роутер - они лежат в папке pages
// компоненты не делятся на умные/глупые, любой компонент может быть привязан к стору
// способ передачи пропсов в компонент остается на усмотрение разработчика,
// но если компонент берет что-то из стора, то он не должен получать данные, которые есть в сторе, пропсами от родителя
// при необходимости можно делать отдельно экспорт глупого компонента и умного
@connect(mapStateToProps)
@css(styles)
export default class Example extends React.Component {
  // описание обязательно для всех пропсов
  // сортируем в альфавитном порядке
  static propTypes = {
    /**
     * Диспатчер
     */
    dispatch: PropTypes.func.isRequired,

    /**
     * Данные офера
     */
    data: shapeOfferData.isRequired,

    /**
     * Данные дома
     */
    house: PropTypes.shape({
      address: PropTypes.string,
    }).isRequired,

    /**
     * Id офера
     */
    id: PropTypes.number.isRequired,

    /**
     * Какое-то свойство
     */
    types: PropTypes.arrayOf(PropTypes.string),
  };

  static defaultProps = {
    types: [],
  };

  componentDidMount() {
    const { id, dispatch } = this.props;

    // диспатч вызывает напрямую из компонента,
    // байндим при необходимости передачи колбека в глупый компонент
    dispatch(fetchExtraData(id));
  }

  // порядок методов компонента:
  // - propTypes, defaultProps, state
  // - lifecicle
  // - все остальные
  onOfferDetailsClick = (id) => () => {
    metrics.sendStat(id);
  }

  render() {
    const {
      data: {
        name,
      },
      house: {
        address,
      },
      id,
      types
    } = this.props;

    return (
      <div styleName="offer">
        <h1>{ name }</h1>
        <div className="house">{ address }</div>

        { /* key используется для ререндера компонента в начальном состоянии при изменении id
          (например, переход с одного офера на другой) */ }
        <OfferDetails
          key={ id }
          onClick={ this.onOfferDetailsClick(id) }
        />

        {
          types.map((type) => (
            <div key={ type }>
              { config[type] }
            </div>
          ))
        }
      </div>
    );
  }
}
