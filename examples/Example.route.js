// роут-модуль размещается в папке страницы pages/Page/Page.route.js
import URL from 'constants/url';

// импортим экшен-криаторы из редакс-модуля
import { fetchOffer, fetchOfferSimilar } from './Example.redux';

module.exports = ({ dispatch, getState }) => {
  // объект роута, который потом передается во все компоненты, завязанные на роутер
  const route = {
    // урл страницы определяем через константы
    path: URL.OFFER_CARD,

    // статус страницы, по умолчанию 200
    status: 200,

    getComponent(nextState, callback) {
      // используем require.ensure для асинхронной загрузки бандла страницы
      require.ensure([], (require) => {
        // проверяем статус, если ошибка - рендерим компонент с ошибкой
        callback(null, (route.status === 200
          ? require('./Offer')
          : require('pages/Error'))
        );
      }, 'offer');
    },

    // колбэк захода на роут
    onEnter(nextState, replace, callback) {
      const { params: { id: offerId } } = nextState;

      // все данные, необходимые для отрисовки страницы, запрашивают исключительно в роут-модуле
      // данные, зависящие от пользователя или от клиентского окружения - в componentDidMount страницы
      Promise
        .all([
          dispatch(fetchOffer(offerId)),
          dispatch(fetchOfferSimilar(offerId)),
        ])
        .then(() => {
          // явно выставляем статус страницы 200
          route.status = 200;

          // если надо, меняем тут sdaEventSection
          route.sdaEventSection = route.getSdaEventSection();
          callback();
        })
        .catch((error) => {
          // произошла ошибка. меняем статус страницы на код ошибки
          route.status = Number(error.message) || 500;
          callback();
        });
    },

    // если роут страницы может меняться (меняется location.query), то описываем колбэк на изменение роута
    onChange(prevState, nextState, replace, callback) {
      // ...
    },

    // идентификатор страницы для отправки в кликхаус
    sdaEventSection: 'offerPage',

    // он может быть вычисляемым
    getSdaEventSection() {
      // ...
    },

    /**
     * Колбэк получения данных по роуту для отправки в статистику в кликхаус
     * @param  [object] state - состояние роутера
     * @return [object] объект с данными
     */
    sdaEventCallback(state) {
      const { params: { id: offerId } } = state;
      const offer = getState().offer[offerId];

      if (offer.error) {
        return false;
      }

      const { flatsPriceHistory, data: { rooms } } = offer;

      const flatsCount = flatsPriceHistory && flatsPriceHistory.rooms && flatsPriceHistory.rooms[rooms]
        ? flatsPriceHistory.rooms[rooms].flats.length
        : 0;

      return {
        eventSection: route.sdaEventSection,
        eventData: {
          offerId,
          historyFlatsCount: flatsCount,
          historyAllFlatsCount: flatsPriceHistory && flatsPriceHistory.allFlats
            ? flatsPriceHistory.allFlats.length
            : 0
        },
      };
    },

    /**
     * Колбэк получения данных по роуту для отправки в статистику ga/метрики
     * @param  [object] state - состояние роутера
     * @return [array] массив с данными
     */
    gtmEventCallback(state) {
      const { params: { id } } = state;
      const offer = getState().offer[id];

      if (offer.error) {
        return false;
      }

      const { isNewFlat } = offer.data;

      return [
        isNewFlat ?
          'pageview_offer_new' :
          'pageview_offer_used'
      ];
    },
  };

  return route;
};
