'use strict';

/**
 * Convert one field value to another
 * @param  {int} value: current value
 * @param  {string} a: field name convert from
 * @param  {string} b: field name convert to
 * @param  {array} points
 * @return {int} converted value
 */
const convertAtoB = (value, a, b, points) => {
  let adaptedValue = Number(value || 0);
  const aMin = points[0][a] || 0;
  const aMax = points.length
    ? points.slice(-1)[0][a] || 0
    : 0;

  adaptedValue = Math.max(adaptedValue, aMin);
  adaptedValue = Math.min(adaptedValue, aMax);

  for (let index = 0; index < points.length; index++) {
    const point = points[index];

    if (adaptedValue === point[a]) {
      return point[b];
    }

    if (adaptedValue < point[a]) {
      const pointPrev = points[index - 1];
      const aPointsDelta = point[a] - pointPrev[a];
      const bPointsDelta = point[b] - pointPrev[b];

      return pointPrev[b] + bPointsDelta * (adaptedValue - pointPrev[a]) / aPointsDelta;
    }
  }
};

/**
 * Non-linear slider
 */
class Slider {
  constructor(values, value) {
    this.node = document.createElement('ul');

    this.initEvents()
        .initPoints(values)
        .setPercent(convertAtoB(value, 'value', 'percent', this.points));
  }

  initEvents() {
    this.node.onclick = (event) => this.onClick(event);

    return this;
  }

  /**
   * Convert values to convenient structure 
   * @param  {object} values hash
   * @return {array of objects} [percent/value]
   */
  initPoints(values) {
    this.points = Object
      .keys(values)
      .sort((a, b) => Number(a) - Number(b))
      .map((item) => ({
        percent: Number(item),
        value: values[item],
      }));

    this.points.forEach((...args) => this.renderLabel(...args));

    return this;
  }

  /**
   * onClick hadler. calc new value accoring node offset, update status
   * @param  {event} 
   */
  onClick({ clientX }) {
    const { left } = this.node.getBoundingClientRect();
    const result = 100 * (clientX - left) / this.node.clientWidth;

    this.setPercent(result)
        .setValue(convertAtoB(result, 'percent', 'value', this.points));
  }

  format(value) {
    return `${ value.toFixed() / 1000000 } млн.`;
  }

  getNode() {
    return this.node;
  }

  setValue(value) {
    const p = document.createElement('p');

    p.textContent = this.format(value);

    document.body.appendChild(p);

    return this;
  }

  setPercent(percent) {
    this.node.style.backgroundSize = `${ percent }% 4px, 100% 4px`;

    return this;
  }

  renderLabel({ percent, value }, index) {
    const label = document.createElement('li');

    label.textContent = this.format(value);
    label.style.width = percent
      ? `${ percent - this.points[index - 1].percent }%`
      : `${ percent }%`;

    this.node.appendChild(label);
  }
}