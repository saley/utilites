/**
 * Stack to collect observable components
 */
const stack = [];

/**
 * Return window pixel ratio (to ajust src size when needed)
 * @return {Nimber}
 */
export const getRatio = () => window.devicePixelRatio;

/**
 * Check component is in viewport according offset
 * @param  {React.Component} component
 * @param  {Number} offset
 * @return {Boolean}
 */
export const isComponentVisible = (component, offset) => {
  const { top, height } = component.node.getBoundingClientRect();
  const viewportHeight = window && document ? window.innerHeight || document.documentElement.clientHeight : 0;

  return (top - offset <= viewportHeight) &&
         (top + height + offset >= 0);
}

/**
 * Window onscroll callback
 */
const onScroll = () => {
  stack.forEach(({ component, offset }, index) => {
    if (!isComponentVisible(component, offset)) {
      return;
    }

    stack.splice(index, 1);
    component.onComponentVisible();
  });
}

/**
 * Remove component from stack
 * @param  {React.Component} component
 * @return {Array} stack
 */
export const componentRemoveFromStack = (component) => {
  stack.forEach((item, index) => (item.component === component && stack.splice(index, 1)));

  return stack;
}

/**
 * Add component to stack
 * @param  {React.Component} component
 * @return {Array} stack
 */
export const componentAddToStack = (component) => {
  stack.push(component);

  if (!componentAddToStack.isOnScollInited) {
    componentAddToStack.isOnScollInited = true;

    window.addEventListener('scroll', onScroll);
    window.addEventListener('resize', onScroll);
  }
  
  return stack;
}
