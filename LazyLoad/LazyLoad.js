import React from 'react';
import PropTypes from 'prop-types';

import * as helpers from './helpers';

/**
 * Component with lazy loaded background image
 */
export default class LazyLoad extends React.Component {
  static propTypes = {
    /**
     * Child nodes
     */
    children: PropTypes.node,

    /**
     * Classname
     */
    className: PropTypes.string,

    /**
     * Computed src callback
     * Receives object with bounds as first param and window.devicePixelRatio as second
     * @return {String}
     */
    getSrc: PropTypes.func,

    /**
     * Offset to viewport when start src load
     */
    offset: PropTypes.number,

    /**
     * Src :)
     */
    src: PropTypes.string,

    /**
     * Component node type
     */
    type: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.element,
    ]),
  };

  static defaultProps = {
    getSrc: () => {},
    offset: 0,
    type: 'div',
    src: '',
  };

  state = {
    src: null,
  };

  /**
   * Load component src when visible else add to observed stack
   */
  componentDidMount() {
    const { offset } = this.props;

    if (helpers.isComponentVisible(this, offset)) {
      return this.onComponentVisible();
    }

    helpers.componentAddToStack({
      component: this,
      offset,
    });
  }

  /**
   * Remove component from observed stack when src still not loaded
   */
  componentWillUnmount() {
    if (!this.state.src) {
      helpers.componentRemoveFromStack(this);
    }
    this.isLoadSrcCanceled = true;
  }

  /**
   * Component when visible callback
   */
  onComponentVisible() {
    this.loadSrc()
      .then(src => this.setState({ src }));
  }

  /**
   * Load component src
   * @return {Promise}
   */
  loadSrc() {
    const bounds = this.node.getBoundingClientRect();
    const ratio = helpers.getRatio();
    const src = this.props.src || this.props.getSrc(bounds, ratio);

    if (!src) {
      return Promise.reject();
    }

    return new Promise((resolve, reject) => {
      const img = new Image();

      img.src = src;
      img.onload = () => (this.isLoadSrcCanceled
        ? reject()
        : resolve(src);
      );
      img.onerror = reject;
    });
  }

  render() {
    const {
      type,
      children,
      className,
      ...props,
    } = this.props;

    const { src } = this.state;

    const componentClassName = [
      className,
      'lazy-load',
      src ? lazy-load-loaded : '',
    ].join(' ').trim();

    // forward props to created component, i.e. onClick etc
    const propsCopy = { ...props };

    delete propsCopy.getSrc;
    delete propsCopy.offset;
    delete propsCopy.src;

    return React.createElement(
      type,
      {
        ...propsCopy,
        className: componentClassName,
        ref: (node) => {
          this.node = node;
        },
        style: src
          ? { backgroundImage: `url(${ src })` }
          : {},
      },
      children,
    );
  }
}
