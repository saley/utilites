/**
 * Pagintaion with fixed length
 * @param  {Int} total [pages count]
 * @param  {Int} current [page number]
 * @param  {Int} delta [min count shown links aside current page]
 * @return {Array}
 */
const pagination = ([ total, current, delta ]) => {
  const res = [];
  const len = 2 * delta + 5;
  let currentIndex;
  let i;
  let v;
  
  if (current < len - (delta + 1)) {
    currentIndex = current;
  } else if (total - current <= (delta + 1)) {
    currentIndex = len - (total - current);
  } else {
    currentIndex = delta + 3;
  }
  
  res[1] = 1;
  res[len] = total;
  res[currentIndex] = current;
  
  for(i = currentIndex + 1, v = current + 1; i < len; i++, v++) {
    res[i] = ((i !== len - 1) || (v === total - 1) ) ? v : '...';
  }

  for(i = currentIndex - 1, v = current - 1; i > 1; i--, v--) {
    res[i] = ((i !== 2) || (v === 2) ) ? v : '...';
  }
  
  return res.splice(1);
}
